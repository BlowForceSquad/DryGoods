# 加密算法  

## 对称加密  
> 可逆的算法,可以通过秘钥进行加密解密
### [DES](https://gitee.com/BlowForceSquad/DryGoods/blob/master/EncryptionAlgorithm/src/main/symmetric/DESUtils.java)  

## 非对称加密  
> 不可逆的算法,如MD5, SHA1\SHA-3\SHA-256.. 等Hash算法  
### [MD5](https://gitee.com/BlowForceSquad/DryGoods/blob/master/EncryptionAlgorithm/src/main/asymmetric/MD5Utils.java)  
### [BCrypt](https://gitee.com/BlowForceSquad/DryGoods/blob/master/EncryptionAlgorithm/src/main/asymmetric/BCryptTest.java)  推荐