package main.asymmetric;

/**
 * @author lirenhao
 */
public class BCryptTest {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();

        /*$2a$10$cS3ybUVOf8gnydNonODljO/LJ1tAzX8hA.x5bYyQsocIOm5irjS5K*/
        System.out.println(BCrypt.hashpw("test", BCrypt.gensalt()));

        long endTime = System.currentTimeMillis();
        System.out.println("BCrypt Spend " + (endTime - startTime));

        startTime = System.currentTimeMillis();
        System.out.println("Check "+BCrypt.checkpw("test","$2a$10$cS3ybUVOf8gnydNonODljO/LJ1tAzX8hA.x5bYyQsocIOm5irjS5K"));
        endTime = System.currentTimeMillis();
        System.out.println("BCrypt Spend " + (endTime - startTime));
    }
}
